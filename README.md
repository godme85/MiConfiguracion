# Mis dotfiles y configuración

![Muestra de mi escritorio](https://i.imgur.com/blu1aNH.png)

![Podemos tener abiertas varias ventanas a la vez](https://i.imgur.com/MK4Uer2.png)

En este repositorio subiré mis configuraciones dentro de Linux Mint con i3-gaps. Cualquier duda, mis medios de contacto los pondré en mi sitio web. \#Proximamente.

Twitter: https://twitter.com/godme85  
facebook: https://facebook.com/godme

---

Agregados dos templates que uso para Pandoc en mi .vimrc

---

## Programas que utilizo:

- **Emulador de terminal:** Una modificación de suckless st (simple terminal) realizada por Luke Smith
- **Editor de texto:** Mayoritariamente uso VIM, combinado con Latex, markdown. En ocasiones uso wordgrinder.
- **Reproductor de audio:** cmus, y cvlc
- **Reproductor de vídeo:** VLC.
- **Navegador web:** Dependiendo el Mood, oscilo entre Firefox, qutebrowser y chromium. En Chromium tengo como buscador por default [Duck duck go](http://duckduckgo.com).
- **Hoja de cálculo:** SCIM, LibreOffice Calc. Por momentos debo usar google drive.
- **Presentación:** VIM con Markdown o Rmarkdown. Suckless sent. Un presentador minimalista.
- **Agenda:** calcurse. Compatible con google calendar. Y no, no me sugieran _emacs_.
